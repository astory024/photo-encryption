package ciphers;

public interface Cipher {

	public String encrypt(String msg);
	public String decrypt(String msg);
	public void setKey(int[][] key);
}
