package ciphers;

public class CaeserCipher implements Cipher {
	int [] [] key;
	String result="";
	@Override
	public String encrypt(String msg) 
	{
		result = "";
		char [] text = msg.toCharArray();
		int j = 0;
		int k = 0;
		int sum = 0;
		for(int i = 0; i<msg.length();i++)
		{
			if((int)text[i]>31&&(int)text[i]<127)
			{
				sum = (int)text[i]-32+Math.abs(key[j][k]);	
				text[i]=(char)((((sum%94)+94)%94)+32);
				k=k%key[0].length;
				if(j<key.length-1)
				{
				
					j++;
				}
				else
				{
					k++;
					j=0;
				}
			}
			
		}
		for(int i = 0; i<msg.length();i++)
		{
			result+=text[i];
		}
		
		return result;
	}

	@Override
	public String decrypt(String msg) 
	{
		
		result = "";
		char [] text = msg.toCharArray();
		int j = 0;
		int k = 0;
		int difference = 0;
		for(int i = 0; i<msg.length();i++)
		{
			if((int)text[i]>31&&(int)text[i]<127)
			{
				difference = (int)text[i]-32-Math.abs(key[j][k]);
				text[i]=(char)((((difference%94)+94)%94)+32);
				k=k%key[0].length;
				if(j<key.length-1)
				{
					j++;
				}
				else
				{
					k++;
					j=0;
				}
			}
		}
		
		for(int i = 0; i<msg.length();i++)
		{
			result+=text[i];
		}
		return result;

	}
	@Override
	public void setKey(int[][] key)
	{
		this.key = key;
	}

}
