package application;

import ciphers.Cipher;

public class MessageHandler {
	Cipher c;
	String input;
	int [] [] key;
	public MessageHandler(Cipher c, String input)
	{
		this.c = c;
		this.input = input;
	}
	public void setInput(String input)
	{
		this.input = input;
	}
	public void setCipher(Cipher c)
	{
		this.c = c;
	}
	public String encryptMsg()
	{
		return c.encrypt(input);
	}
	
	public String decryptMsg()
	{
		return c.decrypt(input);
	}
	public void setKey(int[][]key)
	{
		this.key = key;
		c.setKey(key);
	}
	
}
