package application;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class PhotoHandler {
	BufferedImage image;
	int height;
	int width;
	int [] [] pixelarray;
	
	public PhotoHandler(String filepath)
	{
		try {
			image = ImageIO.read(new File(filepath));
		} catch (IOException e) {
			System.out.println("Image import unsuccessful.");
		}
		if(image!=null)
		{
			height = image.getHeight();
			width = image.getWidth();
			pixelarray = new int[height][width];
			for(int i = 0; i<height; i++)
			{
				for(int j = 0; j<width; j++)
				{
					pixelarray[i][j]=image.getRGB(j, i);
				}
			}
		}
	}
	
	public int[][] getPixelArray()
	{
		return pixelarray;
	}
	
	
}
