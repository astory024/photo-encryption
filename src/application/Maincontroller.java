package application;

import java.awt.Desktop;
import java.awt.FileDialog;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javax.swing.JFrame;

import ciphers.CaeserCipher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Maincontroller implements Initializable {
	
	@FXML TextField filepath;
	@FXML TextArea messagebox;
	@FXML Label infolabel;
	String picpath;
	FileDialog fd;
	MessageHandler mh;
	PhotoHandler ph;
	
	public Maincontroller()
	{
		mh = new MessageHandler(new CaeserCipher(),null);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) 
	{
		
	}
	public void pressBrowse()
	{

		fd = new FileDialog(new JFrame());
		fd.setDirectory("C:\\");
		fd.setVisible(true);
		fd.toFront();
		fd.repaint();
		File[] f = fd.getFiles();
		if(f.length>0)
		{
			if(fd.getFiles()[0].getAbsolutePath().endsWith(".jpg")||fd.getFiles()[0].getAbsolutePath().endsWith(".jpeg")||fd.getFiles()[0].getAbsolutePath().endsWith(".png"))
			{
				picpath = fd.getFiles()[0].getAbsolutePath();
			}
		}
		infolabel.setText("Processing photo...");
		if(picpath!=null)
		{
			filepath.setText(picpath);
			ph = new PhotoHandler(picpath);
		}
		fd.dispatchEvent(new WindowEvent(fd, WindowEvent.WINDOW_CLOSING));
		fd = null;
		infolabel.setText("Ready");
	}
	
	public void pressEncode()
	{
		infolabel.setText("Working...");
		mh.setInput(messagebox.getText());
		mh.setKey(ph.getPixelArray());
		messagebox.setText(mh.encryptMsg());
		infolabel.setText("Done");
	}
	
	public void pressDecode()
	{
		infolabel.setText("Working...");
		mh.setInput(messagebox.getText());
		mh.setKey(ph.getPixelArray());
		messagebox.setText(mh.decryptMsg());
		infolabel.setText("Done");
	}

}
